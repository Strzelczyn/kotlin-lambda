fun main(args: Array<String>) {
    var firstValue = { number1: Int, number2: Int -> number1 + number2 }
    for (i in 0..20) {
        println(firstValue(i, 2))
    }
    var myArray = arrayOf("z", "a", "a1")
    myArray.asList().stream().filter { i -> i?.get(0) == 'a' }.forEach { t -> println(t) }
}
